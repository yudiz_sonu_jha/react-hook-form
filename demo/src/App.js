import { useState } from 'react';
import { useForm } from 'react-hook-form'; 
import './App.css';


// { } .[ ] 

function App() {

  const{ register,handleSubmit, formState: { errors }}  = useForm();
  const [userInfo,setUserInfo ] =useState();
  const onSubmit =(data)=>{ 
    setUserInfo(data);
    console.log(data);
  } 

  

  return (
    <div className="container">
      <pre>{JSON.stringify(userInfo,undefined,2) }</pre>
  <form onSubmit={handleSubmit(onSubmit)}>
      
    <div className='form'>
      <h1>Registration Form</h1>
      <br></br>
      {/* for username */}
        <div className='c1'>
          <label>FirstName</label>
        <input type="text" 
        placeholder="First name" 
        {...register("First name", {required: true, maxLength: 80})} 
        />
               
         </div>

        <div className='lastname'>
          <label>lastName</label>
          <input type="text" 
          placeholder="Last name" 
          {...register("Last name", {required: true, maxLength: 100})} />
        </div>

        <div className='phone'>
          <label>Mobile </label>
          <input type="tel" 
          className='p'
          placeholder="Mobile number" 
          {...register("Mobile number", {required: true, minLength: 10, maxLength: 12})} />
        </div>

        {errors.exampleRequired && <span>This field is required</span>}
          {/* for email */}
          <br></br>
         <div className='c2'>
          <label>Email</label>
         <input type="text" 
         className='email'
         placeholder="Email" {...register("Email", {required: true, pattern: /^\S+@\S+$/i})} />
        </div>

        {/* for password */}
          <br></br>
         <div className='c3'>
          <label>password</label>
          <input 
          type="password" 
          name='password' 
          placeholder='enter password' 
          className='password' 
          {...register("password", { required: true, maxLength: 20 })}/>
         </div>
         <br></br>
          <div className='gender'>
          <p>Gender</p>
          <div className='G1'>
           <select  {...register("Gender", { required: true })}>
              
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            
            </select>`
          </div>
         </div>
      <button className='btn'>submit</button>   
    </div>
  </form>
    </div>
  );
}

export default App;
